#ifndef FILEUTILS_HPP
#define FILEUTILS_HPP

#include <string>

std::string read_file(std::string& filename);

#endif